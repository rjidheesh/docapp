import 'dart:convert';
import 'dart:io';

import 'package:docapp/main.dart';
import 'package:docapp/utils/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';

var hospid = hUID;
var docid = cUID;
var ctx;
File? _image;


List<String> litems = [];
List<String> patimage = [];
List<String> date = [];
List<String> sessionName = [];
List<String> btid = [];
List<String> blockNo = [];
List<String> slotNo = [];
List<String> presc = [];
List<String> labOrder = [];
List<String> pUID = [];

class Scheduled extends StatefulWidget {
  const Scheduled({Key? key}) : super(key: key);

  @override
  State<Scheduled> createState() => _AnimatedListSampleState();
}

class _AnimatedListSampleState extends State<Scheduled> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  late ListModel<int> _list;
  int? _selectedItem;
  late int
  _nextItem; // The next item inserted when the user presses the '+' button.

  @override
  void initState() {
    super.initState();
    getscheduledlist();
    _list = ListModel<int>(
      listKey: _listKey,
      initialItems: <int>[],
      removedItemBuilder: _buildRemovedItem,
    );
    _nextItem = 1;
  }

  // Used to build list items that haven't been removed.
  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    ctx = context;
    return CardItem(
      animation: animation,
      item: _list[index],
      selected: _selectedItem == _list[index],
      onTap: () {
        setState(() {
          _selectedItem = _selectedItem == _list[index] ? null : _list[index];
        });
      },
    );
  }

  Widget _buildRemovedItem(
      int item, BuildContext context, Animation<double> animation) {
    return CardItem(
      animation: animation,
      item: item,
      selected: false,
      // No gesture detector here: we don't want removed items to be interactive.
    );
  }

  // Insert the "next item" into the list model.
  void _insert(String name, String image, String dateandtime, String session, String slot, String block, String bt, String pres, String lo, String pid) {
    final int index =
    _selectedItem == null ? _list.length : _list.indexOf(_selectedItem!);
    _list.insert(index, _nextItem++);

    litems.insert(index, name);
    patimage.insert(index, BaseURL+'/'+image);
    date.insert(index, dateandtime);
    sessionName.insert(index, session);
    slotNo.insert(index, slot);
    blockNo.insert(index, block);
    btid.insert(index, bt);
    presc.insert(index,pres);
    labOrder.insert(index, lo);
    pUID.insert(index, pid);

  }

  void _remove() {
    if (_selectedItem != null) {
      _list.removeAt(_list.indexOf(_selectedItem!));
      setState(() {
        _selectedItem = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Scheduled'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () { Navigator.pop(context); },
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: AnimatedList(
            key: _listKey,
            initialItemCount: _list.length,
            itemBuilder: _buildItem,
          ),
        ),
      ),
    );
  }


  Future<void> getscheduledlist() async {
    var response = await http.post(Uri.parse(BaseURL+"/pdb/doctor-app/scheduled-lists"), body:{"cUID":docid});
    if(response.statusCode == 200)
      {
        var model = json.decode(response.body);
        if(model['status'] == "success" )
          {
            int len = model['consultations'].length;
            for(int i=0; i<len; i++)
              {
                var temp = model['consultations'][i];
                if(temp['hUID']==hospid)
                  {
                    _insert(temp['patientName'], temp['photo'],temp['date'], temp['vcid'], temp['slotNo'].toString(), temp['blockNo'].toString(), temp['bookingTicketId'], temp['prescription'], temp['labOrder'],temp['pUID']);
                  }
              }
          }
      }
  }
}

typedef RemovedItemBuilder<T> = Widget Function(
    T item, BuildContext context, Animation<double> animation);
class ListModel<E> {
  ListModel({
    required this.listKey,
    required this.removedItemBuilder,
    Iterable<E>? initialItems,
  }) : _items = List<E>.from(initialItems ?? <E>[]);

  final GlobalKey<AnimatedListState> listKey;
  final RemovedItemBuilder<E> removedItemBuilder;
  final List<E> _items;

  AnimatedListState? get _animatedList => listKey.currentState;

  void insert(int index, E item) {
    _items.insert(index, item);
    _animatedList!.insertItem(index);
  }

  E removeAt(int index) {
    final E removedItem = _items.removeAt(index);
    if (removedItem != null) {
      _animatedList!.removeItem(
        index,
            (BuildContext context, Animation<double> animation) {
          return removedItemBuilder(removedItem, context, animation);
        },
      );
    }
    return removedItem;
  }

  int get length => _items.length;

  E operator [](int index) => _items[index];

  int indexOf(E item) => _items.indexOf(item);
}

class CardItem extends StatelessWidget {
  const CardItem({
    Key? key,
    this.onTap,
    this.selected = false,
    required this.animation,
    required this.item,
  })  : assert(item >= 0),
        super(key: key);

  final Animation<double> animation;
  final VoidCallback? onTap;
  final int item;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.headline4!;
    if (selected) {
      textStyle = textStyle.copyWith(color: Colors.lightGreenAccent[400]);
    }
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: onTap,
          child: SizedBox(
            // height: 80.0,
            child: Card(
              elevation: 2,
              color: Colors.greenAccent,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: NetworkImage(patimage[item-1]),
                  ),
                  title: Text(litems[item-1], textAlign: TextAlign.start,),
                  subtitle: Text(date[item-1], textAlign: TextAlign.start, style: TextStyle( color: Colors.white),),
                ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 80,
                        margin: const EdgeInsets.all(1.0),
                        decoration: new BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10))),
                        child: new Column(
                          children: <Widget>[
                            IconButton(
                                onPressed: () async {
                                  EasyLoading.init();
                                  EasyLoading.show(status: 'loading...');
                                  var tem = await callAllStartVCAPIs(item-1);
                                  EasyLoading.dismiss();
                                  var url = BaseURL+'/vcroom/?userType=Doctor&vcType=Normal&sessionId='+sessionName[item-1]+'#'+sessionName[item-1];
                                  _launchURL(url);
                                },
                              icon: Icon(Icons.video_call),
                              splashColor: Colors.greenAccent,
                              tooltip: 'Join VC',
                            ), Text("Join VC")
                          ],
                        ),
                      ),
                      Container(
                        width: 80,
                        margin: const EdgeInsets.all(1.0),
                          decoration: new BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10))),
                        child: new Column(
                          children: <Widget>[
                            IconButton(
                              onPressed: () async {
                                return showDialog(context: ctx, builder: (context)=>AlertDialog(
                                  title: Text("Choose from?"),
                                  actions: <Widget>[
                                    FloatingActionButton(onPressed: () async {
                                      Navigator.pop(context);
                                      bool temp = await getImageCam(item-1);
                                    },
                                        child: Text("Camera")),
                                    FloatingActionButton(onPressed: () async {
                                      Navigator.pop(context);
                                      await getImage(item-1);
                                    }, child: Text ("Gallery"))
                                  ],
                                ),);


                                // await getImage();
                              },
                              icon: Icon(Icons.pending_actions),
                              splashColor: Colors.greenAccent,
                              tooltip: 'Change Prescription',
                            ), Text("Prescription")

                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  callAllStartVCAPIs(int i) async {
    await http.post(Uri.parse(BaseURL+"/web/doctor/consultation-start-update"), body:{"cUID":docid, "bookingId":btid[i], "blockNo":blockNo[i], "slotNo":slotNo[i]});
    await http.post(Uri.parse(BaseURL+"/web/vc/message-start-vc"), body: {"rumid": sessionName[i]});
  }

  Future<void> _launchURL(String s) async {
    await canLaunch(s) ? await launch(s) : throw 'Could not launch $s';
  }

  Future<bool> getImageCam(int index) async {
    bool temp = false;
    final image =  await ImagePicker.pickImage(source: ImageSource.camera);
    _image = image;
    if(_image == null)
    {
      showerror();
      temp =  false;
    }
    else
    {
      await cropImage();
      EasyLoading.show(status: 'Uploading...');
      temp = await uploadprescription(index);
    }
    return temp;
  }

  void showerror() {
    ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
      content: Text("Failed Message"),
    ));
  }

  cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: _image!.path,
      maxHeight: 500,
      maxWidth: 500
    );
    _image = croppedFile;

  }

  Future<bool> uploadprescription(int index) async {
    bool temp =  false;
    List<int> imageBytes = _image!.readAsBytesSync();
    String Base64 = base64Encode(imageBytes);
    var response = await http.post(Uri.parse(BaseURL+"/cdb/doctor-upload-record"), body:{"cUID":docid, "hUID":hUID, "pUID":pUID[index], "bookingTicketId":btid[index], "docName":"prescription", "base64":Base64});
    if(response.statusCode == 200)
    {
      var model = json.decode(response.body);
      EasyLoading.dismiss();
      if(model['status'] == "success" )
      {
        temp = true;
        ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
          content: Text("Prescription Uploaded Successfully"),
        ));
        // getAllSchedules();
      }else
      {
        ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
          content: Text("Failed to upload prescription..."),
        ));
      }
    }else
    {
      ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
        content: Text("Failed to upload prescription..."),
      ));
    }
    return temp;
  }

  getImage(int index) async {
    final image =  await ImagePicker.pickImage(source: ImageSource.gallery);
    _image = image;
    if(_image == null)
    {
      showerror();
    }
    else
    {
      await cropImage();
      EasyLoading.show(status: 'Uploading...');
      await uploadprescription(index);
    }
  }
}