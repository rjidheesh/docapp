import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../Completed.dart';
import '../Scheduled.dart';
import '../main.dart';

String name='';
String det = '';
String image = BaseURL+'/na.png';
var Activeicon = Icons.notifications_active;
var InactiveIcon = Icons.notifications;
var cUID;
var hUID;
var scheduledata = null;
RefreshController _refreshController = RefreshController(initialRefresh: false);





class home extends StatefulWidget
{
  @override
  State<StatefulWidget> createState() => homescreen();
}

class homescreen extends State<home> {

  bool temp = true;
  String drname='';
  String details = '';
  var icon = InactiveIcon;
  String docimage = image;
  var pooled = 0;
  var chat=0;
  var scheduled = 0;
  var completed = 0;

  void _onRefresh() async{
    print("refreshing");
    await checkSchedules();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    _refreshController.loadComplete();
  }

  void update()
  {
    setState(() {
      drname = name;
      details = det;
      icon = Activeicon;
      docimage = image;
    });
  }
  Future <bool> _onBackPressed(){
    exit(0);
  }
  @override
  Widget build(BuildContext context) {
    if(temp)
    {
      temp = false;
      getDoctorName();

    }
    // checkSchedules();
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: new Scaffold(
        resizeToAvoidBottomInset: false,

        drawer: Drawer(
          child: ListView(

            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,

            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Text('Selected Hospital'),
              ),
              ListTile(
                title: Text('Item 1'),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title: Text('Item 2'),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
            ],
          ),
        ),
        body: SmartRefresher(

          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,


          child: StaggeredGridView.count(

            crossAxisCount: 2,
            crossAxisSpacing: 12.0,
            mainAxisSpacing: 12.0,
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            // decoration: BoxDecoration(
            //
            // ),
            // child: Column(

            //
            children: <Widget>[

              Card(

                margin: const EdgeInsets.only(top:30.0, left: 5.0, right: 5.0),
                elevation: 0,
                color: Colors.lightBlueAccent,

                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: ListTile(

                  contentPadding: EdgeInsets.only(left: 10.0, right: 10.0),
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(docimage),
                  ),
                  trailing: Icon(icon),
                  title: Text(drname, textAlign: TextAlign.center, softWrap: true, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
                  subtitle: Text(details, textAlign: TextAlign.center,style: TextStyle( color: Colors.white)),
                ),
              ),
              new Text(""),
              new Text(""),
              GestureDetector(
                onTap: () {
                  print("Clicked pooled");
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => Scheduled()),
                  // );
                },
                child:
                Card(

                  color: Colors.indigoAccent,
                  // elevation: 5,
                  child: Column(

                      children: <Widget>[
                        ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Colors.white,
                            backgroundImage: AssetImage("assets/imgs/pooled.png"),
                          ),
                          title: Text(pooled.toString() , textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white, fontSize: 20.0),),

                        ),
                        new Text("Pooled Consultaion", textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white70, fontSize: 20.0, ),),
                      ]
                  ),


                ),
              ),
              GestureDetector(
                onTap: () {
                  print("Clicked Chat only");
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => ChatOnly()),
                  // );
                },
                child: Card(
                  color: Colors.indigoAccent,
                  // elevation: 5,
                  child: Column(
                      children: <Widget>[
                        ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Colors.white,
                            backgroundImage: AssetImage("assets/imgs/chat.png"),
                          ),
                          title: Text(chat.toString() , textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white, fontSize: 20.0),),
                        ),
                        new Text("Chat Only Consultaion", textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white70, fontSize: 20.0, ),),
                      ]
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  print("Clicked Scheduled");
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Scheduled()),
                  );
                },
                child: Card(
                  color: Colors.indigoAccent,
                  // elevation: 5,
                  child: Column(
                      children: <Widget>[
                        ListTile(
                          leading: CircleAvatar(

                            backgroundColor: Colors.white,
                            backgroundImage: AssetImage("assets/imgs/scheduled.png"),
                          ),
                          title: Text(scheduled.toString() , textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white, fontSize: 20.0),),
                        ),
                        new Text("Scheduled Consultaion", textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white70, fontSize: 20.0, ),),
                      ]
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  // print("Clicked completed");
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Completed()),
                  );
                },
                child: Card(
                  color: Colors.indigoAccent,
                  // elevation: 5,
                  child: Column(
                    children: <Widget>[
                      ListTile(

                        leading: CircleAvatar(
                          backgroundColor: Colors.white,
                          backgroundImage: AssetImage("assets/imgs/completed.png"),

                        ),
                        title: Text(completed.toString() , textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white, fontSize: 20.0),),

                      ),
                      new Text("Completed Consultaion", textAlign: TextAlign.center, softWrap: true, style: TextStyle( color: Colors.white70, fontSize: 20.0, ),),
                    ],

                  ),
                ),
              ),


            ], staggeredTiles: [
            StaggeredTile.extent(2, 100.0),
            StaggeredTile.extent(1, 130.0),
            StaggeredTile.extent(1, 130.0),
            StaggeredTile.extent(1, 130.0),
            StaggeredTile.extent(1, 130.0),
            StaggeredTile.extent(1, 130.0),
            StaggeredTile.extent(1, 130.0),],

            // ),

          ),
          // child:
        ),



      ),
    );
  }


  getDoctorName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getString("hUID"));
    List<String> result = prefs.getString("hUID")!.split(',');
    hUID = result[0];
    print(hUID+ " is the hUID");
    cUID = prefs.getString("cUID");
    print(cUID+" is the cUID");
    // print(prefs.getString("name"));
    name = prefs.getString("name")!;
    int age = prefs.getInt("age")??0;
    image = BaseURL+'/'+prefs.getString("photo")!;
    String age1 = '';
    if(age>0)
      age1 =age.toString();
    else
      age1 = '';
    var gender = prefs.getInt("gender");
    var gender1;
    if(gender==1)
      gender1 = 'Male';
    else
      gender1 = 'Female';
    det = age1+", "+gender1;
    // print("hello1");
    update();
    checkSchedules();
    // print("hello 2");
  }

  Future<void> checkSchedules() async {
    print("Checking schedules "+ cUID);
    // var res = await http.post(Uri.parse(BaseURL+"/pdb/doctor-app/completed-lists"), body:{"cUID":docid});
    var response = await http.post(Uri.parse(BaseURL+"/cdb/doctor/all-days-schedule"), body:{"cUID":cUID});
    print("ddddddddddddddddddddddddddddddd");
    if(response.statusCode == 200)
    {
      var model = json.decode(response.body);
      if(model['status'] == "success")
      {
        scheduled = 0;
        chat = 0;
        completed = 0;
        print("test");
        print(model['consultations']);
        // int total = 2;
        int total = model['consultations'].length;
        for(int i = 0; i<total; i++) {
          var temp = model['consultations'][i];


          if(temp['consultationType'] == "online" || temp['consultationType'] == "offline"  && temp['hUID'] == hUID)
            if(temp['blockNo'] < 100 && temp['status'] == 2 )
            {
              print("scheduled got"+ temp['bookingTicketId']);
              scheduled++;
              updateui("scheduled");
            }
          if(temp['blockNo'] == 300 && temp['hUID'] == hUID && temp['status'] == 2)
          {
            print("chat got");
            chat++;
            updateui("chat");
          }
          if(temp['status'] == 4 || temp['status'] == 7 || temp['status'] == 8)
          {
            print("completed got");
            completed++;
            updateui("completed");
          }

          // print(i);
          print(i.toString()+' '+temp['consultationType']);

        }
      }else{
        print("No data");
      }
      // updateui("type");

    }else
    {
      print("Failed");
    }
  }

  void updateui( String type) {
    setState((){
      if(type == "scheduled")
        scheduled = scheduled;
      if(type=='chat')
        chat=chat;
      if(type=="completed")
        completed = completed;

    });
  }
}


