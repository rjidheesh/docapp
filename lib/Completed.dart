import 'dart:convert';
import 'dart:io';

import 'package:docapp/main.dart';
import 'package:docapp/utils/home.dart';
import 'package:docapp/utils/pdfApi.dart';
import 'package:docapp/utils/pdfViewer.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
class Completed extends StatefulWidget {
  const Completed({Key? key}) : super(key: key);

  @override
  State<Completed> createState() => _AnimatedListSampleState();
}
var hospid = hUID;
var docid = cUID;

List<String> patname = [];
List<String> patimage = [];
List<String> date = [];
List<String> presc = [];


class _AnimatedListSampleState extends State<Completed> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  late ListModel<int> _list;
  int? _selectedItem;
  late int
  _nextItem; // The next item inserted when the user presses the '+' button.

  @override
  void initState() {
    super.initState();
    getCompletedlist();
    _list = ListModel<int>(
      listKey: _listKey,
      initialItems: <int>[],
      removedItemBuilder: _buildRemovedItem,
    );
    _nextItem = 1;
  }

  // Used to build list items that haven't been removed.
  Widget _buildItem(
      BuildContext context, int index, Animation<double> animation) {
    return CardItem(
      animation: animation,
      item: _list[index],
      selected: _selectedItem == _list[index],
      onTap: () {
        setState(() {
          _selectedItem = _selectedItem == _list[index] ? null : _list[index];
        });
      },
    );
  }
  Widget _buildRemovedItem(
      int item, BuildContext context, Animation<double> animation) {
    return CardItem(
      animation: animation,
      item: item,
      selected: false,
      // No gesture detector here: we don't want removed items to be interactive.
    );
  }

  // Insert the "next item" into the list model.
  void _insert( String name, String photo, String datetime, String prescription) {
    final int index =
    _selectedItem == null ? _list.length : _list.indexOf(_selectedItem!);
    _list.insert(index, _nextItem++);
    patname.insert(index, name);
    patimage.insert(index, photo);
    date.insert(index, datetime);
    presc.insert(index, prescription);

  }

  // Remove the selected item from the list model.
  void _remove() {
    if (_selectedItem != null) {
      _list.removeAt(_list.indexOf(_selectedItem!));
      setState(() {
        _selectedItem = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Completed'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () { Navigator.pop(context); },
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: AnimatedList(
            key: _listKey,
            initialItemCount: _list.length,
            itemBuilder: _buildItem,
          ),
        ),
      ),
    );
  }

  Future<void> getCompletedlist() async {
    var response = await http.post(Uri.parse(BaseURL+"/pdb/doctor-app/completed-lists"), body:{"cUID":docid});
    if(response.statusCode == 200)
      {
        var model = json.decode(response.body);
        if(model['status'] == "success" )
          {
            int len = model['consultations'].length;
            for(int i=0; i<len; i++)
              {
                var temp = model['consultations'][i];
                if(temp['hUID']==hospid)
                  {
                    _insert(temp['patientName'], temp['photo'],temp['date'], temp["prescription"]);
                  }
              }
          }
      }
  }
}

typedef RemovedItemBuilder<T> = Widget Function(
    T item, BuildContext context, Animation<double> animation);

class ListModel<E> {
  ListModel({
    required this.listKey,
    required this.removedItemBuilder,
    Iterable<E>? initialItems,
  }) : _items = List<E>.from(initialItems ?? <E>[]);

  final GlobalKey<AnimatedListState> listKey;
  final RemovedItemBuilder<E> removedItemBuilder;
  final List<E> _items;

  AnimatedListState? get _animatedList => listKey.currentState;

  void insert(int index, E item) {
    _items.insert(index, item);
    _animatedList!.insertItem(index);
  }

  E removeAt(int index) {
    final E removedItem = _items.removeAt(index);
    if (removedItem != null) {
      _animatedList!.removeItem(
        index,
            (BuildContext context, Animation<double> animation) {
          return removedItemBuilder(removedItem, context, animation);
        },
      );
    }
    return removedItem;
  }

  int get length => _items.length;

  E operator [](int index) => _items[index];

  int indexOf(E item) => _items.indexOf(item);
}

/// Displays its integer item as 'item N' on a Card whose color is based on
/// the item's value.
///
/// The text is displayed in bright green if [selected] is
/// true. This widget's height is based on the [animation] parameter, it
/// varies from 0 to 128 as the animation varies from 0.0 to 1.0.
class CardItem extends StatelessWidget {
  const CardItem({
    Key? key,
    this.onTap,
    this.selected = false,
    required this.animation,
    required this.item,
  })  : assert(item >= 0),
        super(key: key);

  final Animation<double> animation;
  final VoidCallback? onTap;
  final int item;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: SizeTransition(
        axis: Axis.vertical,
        sizeFactor: animation,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: onTap,
          child: SizedBox(
            // height: 80.0,
            child: Card(
              elevation: 2,
              color: Colors.lightBlueAccent,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: NetworkImage(BaseURL+'/'+patimage[item-1]),
                    ),
                    title: Text(patname[item-1], textAlign: TextAlign.start,),
                    subtitle: Text(date[item-1], textAlign: TextAlign.start,style: TextStyle( color: Colors.white)),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.all(1.0),
                        decoration: new BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: new Column(
                          children: <Widget>[
                            IconButton(
                              splashColor: Colors.greenAccent,
                                onPressed: () async {
                                if(presc[item-1] == "na.jpg")
                                  {
                                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Prescription Not Found")));
                                  }else{
                                  final url = BaseURL + presc[item-1];
                                  final file = await PDFApi.loadNetwork(url);
                                  openPDF(context, file, "Prescription");
                                }
                                },
                                icon: Icon(Icons.pending_actions)
                            ),
                            Text("Prescription")
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void openPDF(BuildContext context, File file, String s) => Navigator.of(context).push(
    MaterialPageRoute(builder: (context) => PDFViewerPage(file: file, title: s)),
  );

}
