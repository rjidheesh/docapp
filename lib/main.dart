import 'dart:convert';

import 'package:docapp/utils/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

void main() {
  runApp(MyApp());
}
final _text = TextEditingController();
final password = TextEditingController();
bool _validate = false;
bool valpwd = false;
bool _value = false;
var model = null;
// String BaseURL = "http://10.0.2.2:3017";
// String BaseURL = "http://192.168.42.191:3017";
// String BaseURL = "https://drtop.in";
String BaseURL = "https://test.etherapy.in";

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MyDoc',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'MyDoc for Doctors', key: null,),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({ Key? key,  required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    _checkdata();
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(padding: EdgeInsets.all(10),
            child: TextField(
              controller: _text,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Mobile Number',
                hintText: 'Please enter your mobile number here',
                errorText: _validate?'Value Can\'t Be Empty' : null,
              ),
            ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                controller: password,
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                  hintText: 'Enter your secure password',
                  errorText: valpwd?'Value Can\'t Be Empty' : null,
                ),
              ),
            ),
            CheckboxListTile(
              title: const Text('Licence Agreement'),
              subtitle: const Text('Agree licence agreement to continue'),
              autofocus: false,
              selected: _value,
              value: _value,
              onChanged: (bool? value) {
                setState(() {  _value = !_value;  });

              },
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
                shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
              ),
              onPressed: (){},
              child: Text('Forget Password', style: TextStyle(color: Colors.blue, fontSize: 15), ),

            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                color: Colors.blue, borderRadius: BorderRadius.circular(20)
              ),
              child: ElevatedButton(
                onPressed: () async{
                  _text.text.isEmpty ? _validate = true : _validate = false;
                  password.text.isEmpty ? valpwd = true : valpwd = false;
                  if(!_validate && !valpwd)
                    {
                      if(_value)
                        {
                          EasyLoading.show(status: 'Loading...');
                          model = await postData(BaseURL+'/cdb/doctor-login', _text.text , password.text);
                          if(model!=null)
                            {
                              await WriteToDb(model);
                              print(model['cUID']);
                              if(model['status'] == "success")
                                {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => home()),
                                  );
                                }else
                                  {
                                    Toast.show(
                                        "Failed to Login!", context, duration: Toast
                                        .LENGTH_LONG,
                                        backgroundColor: Colors.white,
                                        textColor: Colors.red,
                                        gravity: Toast.BOTTOM);
                                  }
                            }else{
                            print("Login failed connection error");
                          }
                          EasyLoading.dismiss();
                        }else
                          {
                            Toast.show("Please Agree Licence to continue!", context, duration: Toast.LENGTH_LONG, backgroundColor: Colors.blue, textColor: Colors.white , gravity:  Toast.BOTTOM);
                          }
                    }else
                      {
                        print("Failed");
                      }
                }, child: Text(
                'Login',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
              ),
            )
          ],
        ),
      )
    );

  }

  postData(String url, String mobileNo, String Password) async {
    var response = await http.post(Uri.parse(url), body:{"mobileNo":mobileNo, "password":Password});
    // print(response.statusCode);
    if(response.statusCode == 200)
    {
      String str = response.body;
      var model = json.decode(response.body);
      return model;
    }else
    {
      return null;
    }
  }

  WriteToDb(model) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // print(model.cUID);
    await prefs.setString("cUID", model['cUID']);
    var hUID = model['hUID'].join(',');
    // print(hUID);
    await prefs.setString("hUID", hUID);
    await prefs.setString("name", model['name']);
    await prefs.setString("mongo", model['mongoId']);
    await prefs.setString("photo", model['photo']);
    await prefs.setString("registernumber", model['registerNo']);
    await prefs.setString("address", model['address']);
    await prefs.setString("mobile", model['mobileNo']);
    await prefs.setInt("gender", model['gender']);
    await prefs.setInt("age", model['age']);
    await prefs.setString("hcUID", model['hcUID'].join(','));
    await prefs.setString("qualifications", model['qualifications'].join(','));
    await prefs.setString("speciality", model['specialty'].join(','));
    String hospnames = json.encode(model['hospitalNames']);
    await prefs.setString("hospitalNames", hospnames);
  }

  Future<void> _checkdata() async {
    print("trying to go to next screen");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var cUID = prefs.getString("cUID")??null;
    if(cUID!=null)
    {
      print("trying to go to next screen with data");
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => home()),
      );
    }else{
      print("trying to go to next screen no data");
    }
  }


}
